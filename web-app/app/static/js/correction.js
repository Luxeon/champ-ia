let form = document.querySelector(".correct-form")
let otherRadio = document.querySelector('#radio-mushroom-other');
let otherTextInput = document.querySelector('#correct-form-text');
let backButton = document.querySelector('.back');

backButton.addEventListener('click', (e) => {
    e.preventDefault();
    window.history.back();
})

form.addEventListener('change', (e) => {
    if (otherRadio.checked === true) {
        otherTextInput.removeAttribute("disabled");
    } else {
        otherTextInput.setAttribute("disabled", "");
    }
})
