import tensorflow as tf
from tensorflow import keras
import numpy as np
from PIL import Image, ExifTags

# Load image, resize and convert it to an array
# 3 resize modes : stretch (default), fill_black, fill_pixel
def load_image(path, resize_mode="stretch", size=256):

    # Image resize at load only if stretch mode
    target_size = None
    if resize_mode == "stretch":
        target_size = (size, size)

    img = keras.preprocessing.image.load_img(path, target_size=target_size)

    print(img.height, img.width)

    for orientation in ExifTags.TAGS.keys():
        if ExifTags.TAGS[orientation]=='Orientation':
            break
    
    exif = img._getexif()

    if exif:
        if exif[orientation] == 3:
            img=img.rotate(180, expand=True)
        elif exif[orientation] == 6:
            img=img.rotate(270, expand=True)
        elif exif[orientation] == 8:
            img=img.rotate(90, expand=True)
    
    # Get width or height based on initial aspect ratio for the new max size
    if img.height > img.width:
        h = size
        w = int(size * img.width / img.height)
    else:
        h = int(size * img.height / img.width)
        w = size
    
    resized_img = img.resize((w, h))
    
    # Convert to array and add columns or rows based on resize_mode to get a square matrix 
    img_array = keras.preprocessing.image.img_to_array(resized_img)

    keras.preprocessing.image.save_img(path, img_array)
    
    if h == w:
        return img_array
    elif h > w:
        axis = 1
        zero_matrix = np.zeros((h, h - w, 3), dtype=np.int8)
        if resize_mode != "fill_black":
          last_col = img_array[:, -1, :]
          last_col = np.expand_dims(last_col, axis=1)
          zero_matrix = last_col + zero_matrix
    else:
        axis = 0
        zero_matrix = np.zeros((w - h, w, 3), dtype=np.int8)
        if resize_mode != "fill_black":
          zero_matrix = zero_matrix + img_array[-1]

    final_img_array = np.concatenate((img_array, zero_matrix), axis=axis)

    # Replace the original image with the new processed image (only if not stretch)
    if not resize_mode == "stretch":
        keras.preprocessing.image.save_img(path, final_img_array)
        
    return final_img_array