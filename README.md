Champ'IA is a web application of fungus recognition based on Artifical Intelligence. The application can recognize 6 types of mushrooms: Amanita Bisporigera, Amanita Muscaria, Boletus Edulis, Cantharellus and Omphalotus Olearius thanks to Deep Learning. The ultimate goal is to find out whether the fungus is edible or not.

This is a group project carried out during my 4th year of study. The creation of the detection model was done in **Python** with the Keras and Tensorflow libraries and the web application was developed with Flask. The project is available for testing purpose on my portfolio just right [here][1].

It is strongly recommended to test Champ'IA on smartphone in order to take advantage of the camera.

*Warning!* This is a technical demonstration and should not be used in real conditions. The consumption of unknown mushrooms can be dangerous for health.

How to use it ?
---------------

* The only thing you have to do is to upload a picture or to take a photo if your are on a smartphone and validate.
* After going into the model, you can see the result with a confidence index.
* You can also see the probabilities returned by the model for each class.
* If you are a specialist and you know at 100% that Champ'IA is wrong on a detection, you can correct it by chossing to which class the murshroom should be associated. This is useful to collect data and improve the model.

Installation
------------

It is possible for you to test the application by yourself locally. For this, the source files are at your disposal.

You also have a `docker-compose.yml` file allowing you to launch the application in a Docker container on port 8080.

Creation
--------

This web application use a fixed model to make the prediction but we have to make this one before this final product.

You can find our dataset and the Jupyter notebook we use to create our model. You can find all the idea logic behind the treatment of the pictures before the model training in it. However, the document is only available in French.

About me
--------

I am a Computer Science Master student. You can find some of my other projects on my [Portfolio][2]. 

[1]: https://champia.cyrilpiejougeac.dev
[2]: https://portfolio.cyrilpiejougeac.dev
